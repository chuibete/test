import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class RaceService {

  mainUrl: string = 'http://ponyracer.ninja-squad.com';
  requestUrl: string = '/api/races?status=PENDING';

  constructor(private http: Http) { }

  list() {
    return this.http.get(`${this.mainUrl}${this.requestUrl}`)
      .map(res => res.json());
  }
}
