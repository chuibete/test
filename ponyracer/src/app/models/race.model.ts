import { PonyModel } from './pony.model';

export class RaceModel {
  id: number;
  name: string;
  ponies: Array<PonyModel>;
}
