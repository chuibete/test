Angular Training

David Barreto
Solutions Architect @ Rangle.io
Github: github.com/barretodavid
Blog: david-borreto.com 

1. Intro {
	bootstrap {
		import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
		import { enableProdMode } from '@angular/core';
		import { AppModule } from './app/app.module';
		
		if (process.env.NODE_ENV === 'production') {
			enableProdMode();
		}
		
		platformBrowserDynamic().bootstrapModule(AppModule);
	}
	
	4 type of module : root module, feature module, buit-in modules (browserModule, FormsModule, ...), Third party Modules (AngularMaterial)
	a root module don't have the export property
	Only the root module diffeine the bootstrap property
	All service are mainly defined in the commonModule and the other module add the commonModule in theyre import
	
	Module {
		import { NgModule } from '@angular/core';
		@NgModule({
			imports: [ ... ], //other features
			declarations: [ ... ], //component, pipe, directive
			providers: [ ... ], // services
			exports: [ ... ], // to make wath is in declarations public
			bootstrap: [ ... ] // Root component
		})
		export class MyModule{}
	}
	
	Component {
		import { Component } from '@angular/core';
		
		@Component({
			selector : 'rio-widget',
			styles: ['p {color: red]'], //le style ici ne sera pas transferer au child
			styleUrls: ['app.component.css'],
			template: '',
			templateUrl: 'app.component.html'
		})
		export class WidgetComponent {
			
		}
	}
}

2. Creating a hello world application {
	
}

3. Components In Depth {
	data from parent to child {
		 @Component({
			 selector: 'rio-app',
			 template: '<rio greeter [name]="varName"></rio-greeter>'
			 template: '<rio greeter [name]="'John'"></rio-greeter>'
			 template: '<rio greeter name="John"></rio-greeter>'
		 })
		 export class AppComponent {
			 varName = 'John';
		 }
		 
		 @Component({
			 selector: 'rio-greeter',
			 template: '<p> Hello, {{name}}</p>'
			 template: '<p> Hello, {{surname}}</p>'
		 })
		 export class GreeterComponent {
			 @Input() name: string; //recup la var name donner par le parent
			 @Input('name') firstname: string; //recup la var name donner par le parent et le nome surname
			 
		 }
	}
	data from child to parent {
		@Component({
			selector: 'rio-counter',
			template:`
<div>
	<p> Child Count: {{ count }}</p>
	<button (click)="increment()">Increment</button>
</div>
`			
		})
		export class CounterComponent {
			@Input() count = 0;
			@Output() countChange = new EventEmitter<number>()
			
			increment(): void {
				this.count++;
				this.countChange.emit(this.count);
			}
		}
		
		@Component({
			selector: 'rio-app',
			template:`
<p> Parent Count: {{ parentCount }}</p>
<rio-counter (countChange)="onCountChange($event)"></rio-counter>
`
	})
	export class AppComponent {
		parentCount: number;
		
		onCountChange(count: number): void {
			this.parentCount = count;
		}
	}
	
	Two-way data binding {
		<rio-counter [count]="count" (countChange)="count=$event"></rio-counter>
		<rio-counter [(count)]="count"></rio-counter>
	}
	
	Template Projection {
		@Component({
			selector: 'rio-app',
			template: `
				<rio-child>
					<p>Projected content</p>
				</rio-child>
			`
		)}
		export class AppComponent {}
		
		@Component({
			selector: 'rio-child',
			template: `
				<h4>Child Component</h4>
				<ng-content></ng-content>
			`
		)}
		export class ChildComponent {}
	}
	
	Multi Template Projection Area {
		<!-- parent -->
		<rio-child>
			<header><p>This is my header content</p></header>
			<footer><p>This is my footer content</p></footer>
		</rio-child>
		
		<!-- child -->
		<h4>Child component </h4>
		<ng-content select="header"></ng-content> <!-- le selcteur peut etre sur element, .class ou #id -->
		<ng-content select="footer"></ng-content>
	}
}

4. Directives {
	ngStyle {
		template: `
			<p style=""padding: 1rem"
				[ngStyle]="{
					color: 'red',
					'font-weight': 'bold',
					borderBottm: borderStyle
				}"
			>
		`
		borderStyle = '1px solid block';
	}
	ngClass {
		template: '<p ngClass="centered-text underlined" class="orange"></p>', 
		
		template: '<p [ngClass]="classes" class="orange"></p>',
		classes = 'centered-text underlines';
		classes = ['centered-text', 'underlinded'];
		
		template: '<p [ngClass]="{'centered-text': isCentered, underlined: isUnderlined}" class="orange"></p>',
		isCentered = true;
		isUnderlined = true;
	}
	
	*ngIf(isTrue)
	*ngFor(let class of classes)
		<rio-episode *ngFor="let episode of episodes" [episode]="episode"></rio-episode>
		<rio-episode *ngFor="let episode of episodes; let i = index; let isOdd = odd" [episode]="episode"
			[ngClass]="{odd: isOdd}"></rio-episode>
		index, first, last, even, odd
	*ngSwitch
		<div class="tabs-selection">
			<tab [active]="isSelected(1)" (click)="setTab(1)">Tab 1</tab>
			<tab [active]="isSelected(2)" (click)="setTab(2)">Tab 2</tab>
		</div>
		<div [ngSwitch]="tab">
			<tab-content *ngSwitchCase="1">Tab Content 1</tab>
			<tab-content *ngSwitchCase="2">Tab Content 2</tab>
			<tab-content *ngSwitchCaseDefault>Select a tab</tab>
		</div>
		
		@Component({})
		export class AppComponent {
			tab = 0;
			setTab(num: number): void {this.tab = num; }
			isSelected(num: number): boolean { return this.tab === num; }
		}
		
}

5. Pipes {
	lowercase, currency, async, decimal, json, slice, currency, uppercase, date, percent, i18nplural
	
	Async {
		
	}
}

6. Services {
	import { Injectable } from '@angular/core';
	
	@Injectable()
	export class MyService {
		
	}
	
	constructor (private myService: MyService
}


FIN JOUR 1

7. Miscellanous Topics {
	
}

8. Forms {
	
}

9. Routing {
	
}

10. Unit Testing {
	
}