----- Npm -----

npm uninstall -g angular-cli
npm cache clean
npm i -g angular-cli@2.4.5

----- Angular -----
ng new
ng serve
ng test
ng test --single-run
ng e2e