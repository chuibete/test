Android Studio

-> installer npm et node
-> npm install -g react-native-cli
-> react-native init MyFirstProject

Installer Android Studio
-> instaler Java SE Development Kit 8
-> SDK Manager (Default Seting > Systeme Setting > Android SDK)
  ->Launch Standalone SDK Manager 
  -> cocher :
    - Intel et SDK Platform des versions a travailler
    - extra : intel x86 Emulator Accelarator, Google USB Driver, Android Support Repo/librairy
-> AVD Manager
  -> Lancer le device que l'on veut simuler
  -> fermer android studio sans fermer la simulation
  
Ajout : https://software.intel.com/en-us/android/articles/intel-hardware-accelerated-execution-manager/
  
-> installer sublimeText3
  -> https://packagecontrol.io/installation#st3
  -> ctrl+maj+p
    ->Control Package: Install Package
      -> Babel
  -> View 
    -> syntaxe 
      -> open all with current extension as ...
        -> Babel 
          -> Javascript (Babel)
-> cmd -> aller dans le dossier du projet
  -> react-native run-android
  -> react-native start
    