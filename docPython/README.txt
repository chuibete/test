# Project Name

Sphinx documentation

## Installation

Installer Python 2.7
Installer pip (s'il n'est pas pckagé avec python)
Dans l'invite de commande executer : pip install Sphinx
Pour demarrer un nouveau projet, toujours dans l'invite de commandes : sphinx-quickstart

Pour la redaction de la doc, cf. : http://www.sphinx-doc.org/en/stable/tutorial.html

## Usage

Pour compiler en ligne de commande ajouter les lignes suivantes dans le fichier MAKEFILE 

SPHINXBUILD   = sphinx-build
SPHINXPROJ    = [nom du projet (ex: SoftLink_USM)]
SOURCEDIR     = [dossier source (par defaut : source)]
BUILDDIR      = [dossier de destination (par defaut : build)]

pdf:
	$(SPHINXBUILD) -b pdf $(SOURCEDIR) $(BUILDDIR)/pdf
    @echo
    @echo "Build finished. The PDF files are in _build/pdf."

Pour lancer la compilation dans l'invite de commande : 
make html 	-> sortie au format htm
make pdf 	-> sortie au format pdf en accord avec les options definies dans le MAKEFILE


Pour plus d'infos 
https://docs.typo3.org/typo3cms/extensions/sphinx/fr-fr/AdvancedUsersManual/RenderingPdf/Index.html

## Dependances 

rst2pdf : pour exporter direct en pdf (sans passer par latex)
``pip install sphinx_rtd_theme``
http://rst2pdf.ralsina.me/handbook.html


apres peut y avoir aussi :

- le theme de la doc si tu en prends un qui n'est pas standard
- l'extension pour la lecture des animations swf (https://sphinxcontrib-swf.readthedocs.io/en/latest/)

