.. _airlink-config-vocal:

**********************************
AirLink Vocal Announcements update
**********************************

Here is the procedure to change the vocal announcements version on your AirLink equipment using SoftLink.

**Step 1** Once your equipment is connected to your computer, click on **Refresh** to update the available COM ports list,

**Step 2** Go to *Vocal Announcements* tab.

**Step 3** Click on **Open file** to open windows explorer. Select the MEM file corresponding to the version you'd like to upload. This will unlock the **Upload file** button.

**Step 4** In the notification panel you will see which file has been opened. If it's correct, click on **Upload file**.

**Step 5** Wait until the file is uploaded. Once it's done the message "Update OK" should appear in the notification panel. 

You can now either disconnect your equipment or proceed to the update of the parameters.

.. raw:: html

   <iframe width="600" height="600" src="_swf/Vocal_Config.html" frameborder="0" allowfullscreen></iframe>
   