.. SoftLink_USM documentation master file, created by
   sphinx-quickstart on Fri Mar 10 09:31:22 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _index:
   
************************
About This Documentation
************************

Welcome to the official documentation for the SoftLink software !

.. image:: _static/SoftLink2085_Main.png 

SoftLink is an all-in-one configuration tool for Globalsys equipment from AirLink 2085 product range.

It works on Windows and is suitable for every AirLink 2085 products with an integrated software version 17 or higher. 

If you are starting out with SoftLink, read the :ref:`basic-overview` section first.

If you already know your way around SoftLink, you can always go to the :ref:`airlink-config-p1` section to refresh your memories on the various procedure.

.. note::
	In the case you should experience troubles using SoftLink, please check the :ref:`reporting-bugs` section. 
	
Contents
========
.. toctree::
   :maxdepth: 2 
   
   installation.rst
   basic_overview.rst
   airlink_config.rst
   airlink_config_param.rst
   airlink_config_vocal.rst
   airlink_config_soft.rst
   tutorials.rst

.. toctree::
   :hidden:
 
   license.rst 
   bugs.rst
   copyright.rst
   