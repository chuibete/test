.. _reporting-bugs:

**************
Reporting Bugs
**************

In order to continue to offer you a pleasant user experience with SoftLink, the engineers would like to
know of any deficiencies you may find during your use of SoftLink.

Documentation bugs
==================

If you find a bug in this documentation or would like to propose an improvement,
please send an e-mail to technical@globalsys.fr describing the bug and where you found
it.

.. note:: 
	technical@globalsys.fr is a mailing list run by the R&D team; your request will be
	noticed, even if it takes a while to be processed.

.. seealso::
   `How to Report Bugs Effectively <http://www.chiark.greenend.org.uk/~sgtatham/bugs.html>`_
      Article which goes into some detail about how to create a useful bug report.
      This describes what kind of information is useful and why it is useful.

   `Bug Writing Guidelines <http://developer.mozilla.org/en/docs/Bug_writing_guidelines>`_
      Information about writing a good bug report.  Some of this is specific to the
      Mozilla project, but describes general good practices.

