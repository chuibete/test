.. _installation:

************
Installation
************

SoftLink is a 32-bit portable software. No installation is needed. 

You just have to keep all the files required by SoftLink in the same folder. 
This folder can be moved around your computer (or thumb drive) and the software and its extensions will still work. 

Getting SoftLink 4.6
********************

To obtain the download link of the application, please send an e-mail to sales@globalsys.fr.

.. note:: 
	The Globalsys technical support team will informe you of newest release, and
	will provide you the link to get this new version. 
	
How to Install the software
***************************

1. Download the compressed files
2. Unzip them to the folder of your choice. 
	
You will find the *SoftLink 4.6.exe* executable inside that folder.

Release Channels
****************

At the time of this writing, the only release of SoftLink existing is version 4.6.