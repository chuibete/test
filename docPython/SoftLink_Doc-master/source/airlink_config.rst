.. _airlink-config-p1:

**********************************
AirLink Hardware Preparation
**********************************

This sections will focus on the essential knowledge you will be needing to : 

* Connect your AirLink 2085 to your computer.
* Modify your equipement settings.
* Select your Vocal Announcements version.
* Upload the newest software version to your equipement.

Prerequisites
***************

* Mini USB cable
* PRG406 programming pack (only for beltpack)

Getting the equipement ready
****************************

.. note::
	* The procedure to connect your equipement to your computer depends on the type of AirLink product you wish to set. Otherwise, the use of SoftLink is identical for both products. 
	* Whether you connect headset or beltpack, the driver will be automatically installed at first connection to your computer. During driver installation LED will flash orange (up to 5 minutes).
	
AirLink 2085 Integrated Headset
-------------------------------

The following steps will guide you through the procedure for integrated headset : 

.. figure:: _static/Headset_step2_plugUSB.png
   :scale: 75 %
   
   **Step 1** On the righ shell, open the rubber protection to plug the USB cable.

.. figure:: _static/Headset_step3_ledBlink.png
   :scale: 75 %
   
   **Step 2** Plug USB cable other half to computer. The LED will start blinking alternatively green and red indicating that the AirLink is correctly connected and identified on the computer.

.. warning::
	If you have version 17, we ask you to remove the batteries of the equipment before pluging the USB cable into the headset.

	To do so, please proceed as shown below : 	

	.. figure:: _static/Headset_step1_removeBattery.png
		:scale: 75 %
		
		On the left shell, open the battery cover. 
		Remove the battery pack or the two rechargeable batteries.
   
AirLink 2085 Beltpack
-------------------------------
   
The following steps will guide you through the procedure for beltpack : 

.. figure:: _static/Mobile_step1_removeBattery.png
   :scale: 75 %
   
   **Step 1** Remove the battery pack by pressing the lockers located on both sides of the equipement.

.. figure:: _static/Mobile_step2_insertProg.png
   :scale: 75 %
   
   **Step 2** Replace it with the programming pack and plug USB cable on the bottom of it.   
 
.. figure:: _static/Mobile_step3_ledBlink.png
   :scale: 75 %
   
   **Step 3** Plug USB cable other half to computer. The LED  will start blinking aternatively green 
   and red indicating that the AirLink is correctly connected and identified on the computer.  
