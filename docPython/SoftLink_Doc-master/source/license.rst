﻿.. _history-and-license:

*******************
History and License
*******************

History of the software
=======================
+---------+------------+--------------+------------+---------+-------+
| Version | Build #    | Derived from | Date       | Platfom | Arch. |
+=========+============+==============+============+=========+=======+
| 4.6     | 6298.25049 | n/a          | 2016-2017  | Windows | x86   |
+---------+------------+--------------+------------+---------+-------+
| 4.6     | 6326.26267 | n/a          | 04/2017    | Windows | x86   |
+---------+------------+--------------+------------+---------+-------+
| 4.6     | 6360.25271 | n/a          | 05/2017    | Windows | x86   |
+---------+------------+--------------+------------+---------+-------+

Terms and conditions for accessing or otherwise using SoftLink
==============================================================

This software is copyrighted by and is the sole property of GLOBALSYS.  
All rights, title, ownership, or other interests in the software remains
the property of GLOBALSYS. 

This software may only be used in accordance with the corresponding license 
agreement. Any unauthorized use, duplication, transmission, distribution, or 
disclosure of this software is expressly forbidden.      

This Copyright notice may not be removed or modified without prior        
written consent of Globalsys.                                             

GLOBALSYS reserves the right to modify this software without notice.      

Contact information
===================
| Zone d'activités des Petits Carreaux 
| 12, Avenue des coquelicots 
| 94380 BONNEUIL-sur-MARNE - FRANCE
|
| Tel. : +33 1 43 99 42 17 
| Fax : +33 1 43 39 24 19
|
| http://www.globalsys.fr 
| technical@globalsys.fr
|
