.. _copyright:

*********
Copyright
*********

SoftLink Client Edition and this documentation are:

Copyright © 2017 Globalsys. All rights reserved.

-------

See :ref:`history-and-license` for complete license and permissions information.

