.. _airlink-config-soft:

**********************************
AirLink Software update
**********************************

Here is the procedure to update the software on your AirLink equipment using SoftLink.

**Step 1** Go to *Software* tab.

**Step 2** Click on **Upgrade software**.

**Step 3** While *HID USB Bootloader* starts, disconnect the USB cable on the equipment side.

**Step 4a** If you are using the integrated headset, please press **-** on the right shell. 

**Step 4b** If you are using the beltpack, please press the button on the programming pack.

**Step 5** With the adequat button still pressed, reconnect the USB cable on the equipment side. 

**Step 6** If you succeded in step 4 & 5, the notification panel on *HID USB Bootloader* should indicated "Device Attached. Connecting... Device Ready". If not, retry step 3 to 5.

**Step 7** If step 6 is OK, press the **Import Firmware Image** button (File menu or first icon on the left of the toolbar)

**Step 8** Find and select the .hex file corresponding to the software version you whish to upload. A notification of this type, "Opened: sw406v18.hex" , should appear.

**Step 9** Click on **Erase/Program/Verify/Device** (Program menu or second icon on the toolbar) to start upload. 

**Step 10** Wait for transfert to complete. When asked to, disconnect equipment from computer and close *HID USB Bootloader*.

**Step 11** Reconnect equipment to computer and go back to *SoftLink* for further modifications. 

.. raw:: html

   <iframe width="600" height="600" src="_swf/Software_Config.html" frameborder="0" allowfullscreen></iframe>
   