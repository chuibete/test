.. _airlink-config-param:

********************************
AirLink Parameters Modification
********************************
	
Here is the procedure to configure the parameters of your AirLink equipment using SoftLink.

In *General* tab : 

**Step 1** Once your equipment is connected to your computer, click on **Refresh** to update the available COM ports list,

**Step 2** Click on **Get Information** to read the parameters saved in the AirLink,

**Step 3** The window will be updated with the appropriate informations. If the parameters don't show up in the different boxes, please try to do **Get Information** again or to **Refresh**,

**Step 4** You can now modify all the equipment parameters. 

.. raw:: html

   <iframe width="610" height="610" src="_swf/General_Config.html" frameborder="0" allowfullscreen></iframe>
