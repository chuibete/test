.. _basic-overview:

**************
Basic Concepts
**************

Overview
********

To fully understand the rest of this guide, you need to be familiar with the concepts presented in this section.

Navigate through SoftLink
*************************

Few elements compose the graphic interface of this software. 
At launch, SoftLink will open on the *General* tab. 

.. image:: _static/General_Tab.png 

General Tab
===========  

1. Toolbar 
++++++++++
	* **FILE** : Click on **FILE** for restart or Close SoftLink.
	* **HELP** : Gives you access to the **HELP** and the **ABOUT** sections.

2. Serial COM Ports
+++++++++++++++++++

This ensemble is used to get a list of the available COM ports. 
If your equipement is not listed, please press **REFRESH**.

.. seealso::
	If you don't know the COM port used by your equipment, please read the :ref:`animations` section. 

3. Get Information button
+++++++++++++++++++++++++

This button is used to read the parameters from the equipment connected to the computer.
Once you click on it, all the various boxes of the *General* tab will be fulfilled. 

4. Navigation bar
++++++++++++++++++

These are the different tabs you can navigate to in order to fully configure your equipment. 


	* **General** : used for setting the AirLink parameters,
	* **Vocal Announcements** : used to change the audio version of the AirLink,
	* **Software** : used to upgrade the integrated software.
	
.. note::
	Each tab will be documented later on this manual.
	
5. Manufacturer Informations
++++++++++++++++++++++++++++

These information are read-only and reserved to the Globalsys staff. 
Nevertheless, they give some key data on your equipment, such as the Serial number, the integrated software version or the last modification date. 
These informations will be useful in case of equipment malfunction.

6. Equipment Parameters
+++++++++++++++++++++++

There are the information you will be able to change in order to configure your equipment. 

7. Visual Elements
++++++++++++++++++

The visual elements are two : a picture of your product (default value is a black and white "?") and the flag of the vocal announcements version configured.

8. Update button 
++++++++++++++++

This button is used to update your equipment once you are done modifying the parameters. 

9. Progress bar
+++++++++++++++

This element is only useful in the *Vocal Announcements* tab and will give you the possibility to follow the upload progression of the new version file. 

10. Connection informations
+++++++++++++++++++++++++++

The notification zone gives information on whether a device is connected or disconnected.

Vocal Announcements Tab
=======================

Using the navigation bar, you can go to the *Vocal Announcements* tab if you want to change version.
 
E.g: the equipment was configured with french vocal announcements and you want to change it to english. 

.. image:: _static/Vocal_Tab.png 

The notification panel (1), combined with the progress bar in the bottom-left corner, will give you the updates on the upload of the vocal announcements file on your AirLink. 

The two buttons will help you find the file to upload (*Open file* to open a search windows) and to start the upload (*Upload file*).

.. note:: 
	The vocal announcements files are MEM files. (.mem extension)
	
	E.g: Synth2085-008EN.mem is the AirLink 2085 english vocal version. 

Software Tab
=======================

Using the navigation bar, you can go to the *Software* tab if you want to upgrade the integrated software.

.. image:: _static/Software_Tab.png 

To use this feature, SoftLink calls the application for software upgrade *HID USB Bootloader*.

.. image:: _static/HID.png
