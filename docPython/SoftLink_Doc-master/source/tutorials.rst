.. _animations:

********************************
Tutorials
********************************

Video demonstration 
**********************

To see how to configure and use your equipment you can subscribe to the 
`GLOBALSYS channel <https://www.youtube.com/channel/UC7NV9z30kepe-7o16893TfA>`_ on YouTube.

How-to get AirLink serial port COM number 
******************************************

A. Open Control Panel
=======================

On Windows 10
----------------
#. Right-click on the *Start* button.
#. Choose *Control Panel* from the menu that appears. 
#. A control panel window should open.

On Windows 8 or 8.1 
----------------------
#. While on the Start screen, click on the downward-facing arrow icon to bring up the Apps screen.
#. On the Apps screen, scroll to the right and find the Windows System category. 
#. Click on the *Control Panel* icon under Windows System. 
#. Windows 8 will switch to the Desktop and open the Control Panel.

.. note::
	In Windows 10 and Windows 8, the fastest way is through the Power User Menu - just press the **WIN** (Windows) key and the **X** key together. 
	
On Windows 7 or Vista
------------------------
#. Click the *Start* button. 
#. Click *Control Panel* from the list in the right margin.

.. note:: 
	If you don't see *Control Panel* listed, the link may have been disabled as part of a Start Menu customization. 
	Instead, type control in the search box at the bottom of the Start Menu and then click *Control Panel* when it appears in the list above.

3. However you get there, *Control Panel* should open after clicking the link or executing the command.

B. Open Device Manager 
========================

1. What you do next depends on what Windows operating system you're using :
	* In Windows 10 and Windows 8, choose *Hardware and Sound*.
	* In Windows 7, choose *System and Security*.
	* In Windows Vista, choose *System and Maintenance*.

.. note:: 
	If you come from the *Power User Menu* under Windows 10 or 8, select directly *Device Manager* and skip step 2.

2. From this Control Panel screen : 
	* In Windows 10 and Windows 8, check under the *Devices and Printers* heading for *Device Manager*.
	* In Windows 7, look under *System* for *Device Manager*.
	* In Windows Vista, you'll find Device Manager towards the bottom of the window.

#. With Device Manager now open, look for **Ports (COM & LPT)**. If you click on the **+** all the ports currently identified on your computer will be listed here.
#. To know which one is your equipment, please plug the usb cable ends on your AirLink 2085 on one side, and on the computer on the other side. 

.. note::
	If several ports are listed, try to plug and un-plug the usb cable to see which one is appearing/disappearing. 
	